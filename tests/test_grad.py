#!/usr/bin/env python

import theano
import theano.tensor as T
import sys
import numpy as np

import pylab as plt


def main(*args):
	x = T.vector('x')
	W = T.matrix('W')
	f = T.sum(T.nnet.sigmoid(T.dot(W,x)),0)
	# f = 6*x**3 - 2*x**2 +9*x+ 1 + T.cos(x)
	f_prime = T.grad(f,x)
	# f_prime_2 = T.grad(f_prime,x)

	#Then the compiled theano functions for plotting.
	
	F = theano.function(inputs=[x,W], outputs=theano.Out(theano.sandbox.cuda.basic_ops.gpu_from_host(f)))
	F_prime = theano.function(inputs=[x,W], outputs=theano.Out(theano.sandbox.cuda.basic_ops.gpu_from_host(f_prime)))
	# F_prime_2 = theano.function(inputs=[x,W], outputs=f_prime_2)

	#Now let's make a plot.
	weights = np.array([[1,2,3],[4,5,6]], dtype=theano.config.floatX)
	xs = np.random.random(100000)
	xs = np.array(xs,dtype=theano.config.floatX)
	y1 = [F([z,z,z],weights) for z in xs]
	y2 = [F_prime([z,z,z],weights) for z in xs]
	# y3 = [F_prime_2(z,weights) for [z,z,z] in xs]

	print len(xs), len(y1), len(y2)
	# import matplotlib.pyplot as plt
	plt.plot(xs, y1, label='f')
	plt.plot(xs, y2, label='f\'')
	# plt.plot(xs, y3, label='f\'\'')
	plt.plot(xs, [0 for z in xs], label='zero')
	plt.legend()
	plt.show()

	# results, updates = theano.scan(fn=addf,
	#                    outputs_info=[{'initial':x0, 'taps':[-2]}],
	#                    non_sequences=step,
	#                    n_steps=i)

	# f=theano.function([x0,i,step],results)

	if np.any([isinstance(x.op, T.Elemwise) for x in F.maker.fgraph.toposort()]):
		print 'Used the cpu'
	else:
		print 'Used the gpu'
	print F.maker.fgraph.toposort()

if __name__ == "__main__":
	main(sys.argv)