import theano
import theano.tensor as T
import numpy as np

# defining the tensor variables
X = T.matrix("X")
W = T.matrix("W")
b_sym = T.vector("b_sym")

results, updates = theano.scan(lambda v: T.tanh(T.dot(v, W) + b_sym), sequences=X)
compute_elementwise = theano.function(inputs=[X, W, b_sym], outputs=[results])

# test values
x = np.eye(10000, dtype=theano.config.floatX)
w = np.ones((10000, 10000), dtype=theano.config.floatX)
b = np.ones((10000), dtype=theano.config.floatX)
b[1] = 2

print compute_elementwise(x, w, b)[0]

if np.any([isinstance(ax.op, T.Elemwise) for ax in compute_elementwise.maker.fgraph.toposort()]):
	print 'Used the cpu'
else:
	print 'Used the gpu'
print compute_elementwise.maker.fgraph.toposort()
# comparison with numpy
# print np.tanh(x.dot(w) + b)