#!/usr/bin/env python

import theano
import theano.tensor as T
import sys
import numpy as np


def addf(a1,a2):
    return a1+a2

def main(*args):

	i = T.iscalar('i')
	x0 = T.ivector('x0') 
	step= T.iscalar('step')

	results, updates = theano.scan(fn=lambda x,x_1,re: x+x_1+re,
	                   outputs_info=[{'initial':x0, 'taps':[-2,-1]}],
	                   non_sequences=step,
	                   n_steps=i)

	f=theano.function([x0,i,step],results, updates=updates)

	print f([1,1],10000,1)
	print "results", np.asarray(results)
	print "updates", np.asarray(updates)
	if np.any([isinstance(x.op, T.Elemwise) for x in f.maker.fgraph.toposort()]):
		print 'Used the cpu'
	else:
		print 'Used the gpu'
	print f.maker.fgraph.toposort()

if __name__ == "__main__":
	main(sys.argv)