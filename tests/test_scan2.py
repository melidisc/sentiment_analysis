#!/usr/bin/env python

import theano
import theano.tensor as T
import sys
import numpy as np


def main(*args):

	# print np.ones(100)
	seq = theano.shared(np.ones(10**2, dtype=theano.config.floatX),borrow=True)
	seq2 = theano.shared(np.ones(10**2, dtype=theano.config.floatX),borrow=True)
	# seq=T.matrix("seq")
	inp=T.vector('inp')

	out, up = theano.scan(fn= lambda x1,x2,_: x1+x2*_,
				outputs_info=[{'initial':inp, 'taps':[-1]}],
				# non_sequences=inp,
				sequences = [seq,seq2])#,
				# n_steps = seq.shape[0])


	f = theano.function([inp],out,updates=up)
		
	i = np.ones((10**3,1), dtype=theano.config.floatX)
	print f([0])

	if np.any([isinstance(x.op, T.Elemwise) for x in f.maker.fgraph.toposort()]):
		print 'Used the cpu'
	else:
		print 'Used the gpu'
	print f.maker.fgraph.toposort()

if __name__ == "__main__":
	main(sys.argv)