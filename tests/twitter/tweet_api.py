#!/usr/bin/env python

import sys
import os
import json
import pickle
import requests
# from requests import async
import pyhash
# from threading import Thread
from multiprocessing import Process, Queue
# from Queue import Queue
import pdb

class TweetApi():
	def __init__(self,apiKey = None, userName = None, file_name=None):
		self.api_key = apiKey or "f532474211fa8c38bcf0918c231e55ef3df9d07d"
		self.username = userName or "christos"
		self.file_name = file_name or "pickled_tweets.pickle"
		path = os.path.join(os.path.split(os.path.dirname(__file__))[0],
                      self.file_name)
		self.fileObject = open(self.file_name, "w")
		self.url_queue, self.tweet_queue, self.for_save = Queue(), Queue(), Queue()
		self.hasher = pyhash.fnv1a_32()	

	def get_url(self,url_name):
		'''
		return url json content of url
		'''
		r = requests.get(url_name)
		if r.status_code == 200:
			print "Successfull GET request to url ", url_name
			
			return r.json()
		else:
			print "Unsuccessfull GET request to url ", url_name
			return 0
	def parse_tweets(self, dctry):
		twl = []
		for tweet in dctry[u'objects']:
			tw = {}
			hashs = [self.hasher(a) for a in tweet[u'text'].split(" ")]
			print hashs,tweet[u'text']
			tw['text'] = hashs
			tw['date'] = tweet[u'created']
			twl.append(tw)
		return dctry, twl
	def store_results(self, file_name=None, converted_tweets=None):
		
		try:
			pickle.dump(converted_tweets, self.fileObject)
			return True
		except Exception as se :
			print "Failed to pickle the tweets ",se
			return False
	def thread_urls(self):
		#get
		play = True
		while play:
			try:
				url = self.url_queue.get(timeout=1)
			except Exception as e:
				print "Thread URLs empty"
				play = False
				break
			else:

				play = not self.url_queue.empty()
				if not play:
					break
				
				# if not play:
				# 	self.url_queue.join()
				#process
				# pdb.set_trace()
				tweets = self.get_url("http://qualia.org.uk"+url)
				# self.url_queue.task_done()
				#update
				if tweets == 0:
					return 0
				#upgrade
				self.tweet_queue.put(tweets)
		
	def thread_parse(self):
		play = True
		while play:
			try:
				tweets = self.tweet_queue.get(timeout=1)
			except Exception as e:
				print "Parse Tweets empty",e
				play = False
				break
			else:
				play = not self.tweet_queue.empty()
				if not play:
					break
				
				#convert them
				#for every url starting from the above take the next one and parse it, threading
				converted_tweets, data= self.parse_tweets(tweets)
				# self.tweet_queue.task_done()
				if not converted_tweets[u'meta'][u'next'] is None:
					self.url_queue.put(converted_tweets[u'meta'][u'next'])
					self.for_save.put(data)
				else:
					break
				
				print "Go for the next batch ",converted_tweets[u'meta'][u'next']

	def thread_store(self):
		play = True
		while play:
			#get
			try:
				converted_tweets = self.for_save.get(timeout=1)
			except Exception as e:
				print "Store pool empty",e
				play = False
				break
			else:
				play = not self.for_save.empty()
				print "save", play
				if not play:
					break
				#pickle and store them
				if self.store_results(converted_tweets=converted_tweets):
					print "Tweets are saved successfully"
				else:
					print "Tweets lost in Ram"
				# self.for_save.task_done()
def main(*argv):
	
	thread_number = 1
	processes =[] 
	#get the tweets for the site
	api = TweetApi()
	url= "/api/v1/tweet/?username=christos&api_key=f532474211fa8c38bcf0918c231e55ef3df9d07d&limit=20&offset=16520"#"/api/v1/tweet/?api_key=f532474211fa8c38bcf0918c231e55ef3df9d07d&username=christos"
	api.url_queue.put(url)

	for i in xrange(thread_number):
		t1= Process(target=api.thread_urls)
		#t1.deamon = True
		t1.start()
		t2= Process(target=api.thread_parse)
		#t2.deamon = True
		t2.start()
		t3= Process(target=api.thread_store)
		#t3.deamon = True
		t3.start()
		processes.append(t1)
		processes.append(t2)
		processes.append(t3)

	
	#initialize
	print "here"
	sys.stdout.flush()
	

	
	# print "here2"
	# sys.stdout.flush()
	# api.url_queue.join()
	# print "here3"
	# sys.stdout.flush()
	# api.for_save.join()
	# print "here4"
	# sys.stdout.flush()
	# api.tweet_queue.join()
	# print "here5"
	for pro in processes:
		pro.join()
	sys.stdout.flush()

	return 0
if __name__== "__main__":
	try:
		main(sys.argv)
	except KeyboardInterrupt:
		api.store_results()
		print "Exiting.."
