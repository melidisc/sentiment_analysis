#!/usr/bin/env python

import sys
import os
import json
import pickle
import requests
# from requests import async
import pyhash
from threading import Thread
from Queue import Queue
import pdb

class TweetApi():
	def __init__(self,apiKey = None, userName = None, file_name=None):
		self.api_key = apiKey or "f532474211fa8c38bcf0918c231e55ef3df9d07d"
		self.username = userName or "christos"
		self.file_name = file_name or "pickled_tweets_nohash_withdict.pickle"
		path = os.path.join(os.path.split(os.path.dirname(__file__))[0],
                      self.file_name)
		self.fileObject = open(self.file_name, "w")
		self.url_queue, self.tweet_queue, self.for_save = Queue(), Queue(), Queue()
		self.hasher = pyhash.fnv1a_32()
		self.div = 1<<32	
		self.kazani = []
		self.timeout = 2
		self.max=0
		self.dic = {}

	def get_url(self,url_name):
		'''
		return url json content of url
		'''
		r = requests.get(url_name)
		if r.status_code == 200:
			print "Successfull GET request to url ", url_name
			return r.json()
		else:
			print "Unsuccessfull GET request to url ", url_name
			return 0
	def parse_tweets(self, dctry):
		twl = []
		
		for tweet in dctry[u'objects']:
			tw = {}
			# hashs = [(self.hasher(a)/ self.div) for a in tweet[u'text'].split(" ")]
			# if len(hashs) > self.max:
			# 	self.max = (len(hashs))
			tw['text'] = tweet[u'text'].split(" ")#hashs 
			for w in tw['text']:
				try:
					t= self.dic[w]
				except KeyError as e:
					self.dic[w] = 1
				else:
					self.dic[w] +=1
			tw['date'] = tweet[u'created']
			twl.append(tw)
		return dctry, twl
	def store_results(self, file_name=None, converted_tweets=None):
		try:
			#store in temporal while donwloading
			self.kazani.append(converted_tweets)
			return True
		except Exception as se :
			print "Failed to pickle the tweets ",se
			return False

	def save_in_file(self):
		try:
			pickle.dump((self.kazani,self.dic), self.fileObject)
			self.fileObject.close()
			print "Tweets saved in file"
			return True
		except Exception as e:
			print "No save was possible ",e
			return False

	def thread_urls(self):
		#get
		while True:
			try:
				url = self.url_queue.get(timeout=self.timeout)
			except Exception as e:
				print "Thread_urls finished ",e
				break
			else:
				#process
				tweets = self.get_url("http://qualia.org.uk"+url)
				self.url_queue.task_done()
				#update
				if tweets == 0:
					return 0
				#upgrade
				self.tweet_queue.put(tweets)
		
	def thread_parse(self):
		while True:
			try:
				tweets = self.tweet_queue.get(timeout=self.timeout)
			except Exception as e:
				print "Thread_parse finished ",e
				break
			else:
				#convert them
				#for every url starting from the above take the next one and parse it, threading
				converted_tweets, data= self.parse_tweets(tweets)
				self.tweet_queue.task_done()

				if not converted_tweets[u'meta'][u'next'] is None:
					self.url_queue.put(converted_tweets[u'meta'][u'next'])
					self.for_save.put(data)
				else:
					break
				print "\tGo for the next batch ",converted_tweets[u'meta'][u'next']

	def thread_store(self):
		while True:
			#get
			try:
				converted_tweets = self.for_save.get(timeout=self.timeout)
			except Exception as e:
				print "Thread_store finished ",e
				break
			else:
				#pickle and store them
				if self.store_results(converted_tweets=converted_tweets):
					print "Tweets are saved successfully"
				else:
					print "Tweets lost in Ram"
				self.for_save.task_done()
api = TweetApi()				
def main(*argv):
	
	thread_number = 4
	url= "/api/v1/tweet/?api_key=f532474211fa8c38bcf0918c231e55ef3df9d07d&username=christos"
	#url="/api/v1/tweet/?username=christos&api_key=f532474211fa8c38bcf0918c231e55ef3df9d07d&limit=20&offset=16520"#
	api.url_queue.put(url)
	threads =[] 

	for i in xrange(thread_number):
		t1= Thread(target=api.thread_urls)
		t1.deamon = True
		t1.start()
		t2= Thread(target=api.thread_parse)
		t2.deamon = True
		t2.start()
		t3= Thread(target=api.thread_store)
		t3.deamon = True
		t3.start()
		threads.append(t1)
		threads.append(t2)
		threads.append(t3)
	
	#initialize
	#get the tweets for the site
	api.tweet_queue.join()
	api.for_save.join()
	api.url_queue.join()
	for t in threads:
		t.join()
	#save what we have
	api.save_in_file()
	print api.max
if __name__== "__main__":
	try:
		main(sys.argv)
	except KeyboardInterrupt:
		api.save_in_file()
		print "Exiting.."
